<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@page import="java.util.logging.Logger" %>

<html>


<%
    String acc_no = request.getParameter("acc_no");
    if(acc_no != null){
		session.setAttribute("acc_no",acc_no);
		Logger logger=Logger.getLogger(this.getClass().getName());
		logger.info("Logged in as "+acc_no);
%>

<tiles:insert page="/homepage.jsp" flush="true">
    <tiles:put name="title" value="E-Bank" />
    <tiles:put name="header" value="/header.jsp?acc_no=<%=acc_no%>" />
    <tiles:put name="menubar" value="/menubar.jsp?acc_no=<%=acc_no%>" />
    <tiles:put name="content" value="/content.jsp?acc_no=<%=acc_no%>&item_no=1" />
</tiles:insert>

<%
	}
%>
</html>