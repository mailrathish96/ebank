
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	
		
    <script type="text/javascript">
	<%
        
		if(session.getAttribute("acc_no")!=null){
        %>
        window.location.href="home.jsp?acc_no=<%= session.getAttribute("acc_no")%>";
		<%
		}
		%>
        
        $(document).ready(function() {

                $('#login').click(function (e)
                {
                    $.ajax({
                        type: "post",
                        url: "LoginAction.do",
                        data: "acc_no=" +$('#acc_no_login').val()+"&pin_no="+$('#pin_no_login').val(),
                        success: function(msg){
                            console.log('success',msg);

                                $('#login_success').append("Login Successfull!");

                                var form = document.createElement("form");
                                form.setAttribute("method", "post");
                                form.setAttribute("action", "home.jsp");

                                var hiddenField = document.createElement("input");
                                hiddenField.setAttribute("type", "hidden");
                                hiddenField.setAttribute("name", "acc_no");
                                hiddenField.setAttribute("value", $('#acc_no_login').val());
                                form.appendChild(hiddenField);
                                document.body.appendChild(form);
                                form.submit();
                           
                        },
                        error : function(errorThrown) {
                                $('#login_failure').append("login failed! Please try again.");
                        }
                    });
                    e.preventDefault();
                });
            });
        </script>

		
</head>

<body bgcolor="#1abc9c">
<br>
<center>
    <div class="account">

        <center><h2><b>Banking Application</b></h2></center>
        <center><h2><b>Signin.</b></h2></center>
        <input type="text" name="acc_no_login" id="acc_no_login" placeholder="Account number">
        <input type="password" name="pin_no_login" id="pin_no_login" placeholder="Pin number"/>
        <p id ="login_success"></p><p id="login_failure"></p>
        <input type="submit" id="login" value="Login"/>

        <br><br>Don't have an account.<br>
        <a href="CreateAccount.jsp"><u>Signup!</u></a>

    </div>

</center>
</body>
</html>