<%@page import="java.util.logging.Logger" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="json" uri="http://java.sun.com/jsp/jstl/xml" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>E-Banking</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <title>Title</title>
</head>
<body>

<%
    String acc_no = request.getParameter("acc_no");
    if(acc_no != null){
		
	}
	else{%>
		        window.location.href="index.jsp";
	<%}
       %>
<script type="text/javascript">
    $.ajax({
        type: "post",
        url: "GetAccountDetailsAction.do",
        data: "acc_no=<%=acc_no%>",
        success: function(msg){
            document.getElementById("welcome-text").innerHTML = "Welcome "+msg.name;
        },
        error : function(errorThrown) {
            alert(errorThrown);
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#logout').click(function () {
            $.ajax({
                type: "post",
                url: "LogoutAction.do",
                success: function(msg){
					<% Logger logger=Logger.getLogger(this.getClass().getName());
							   logger.info("Logged out from "+acc_no);
							%>
                    parent.location.href="index.jsp";
                    console.log("logged out")
                },
                error : function(errorThrown) {
                    alert(errorThrown);
                }
            });

        });
    });
</script>
<div class="header">
    <div id="main-text"><b>Internet Banking Application</b></div>
    <div id="logout"><b>LOGOUT</b></div>
    <p id="welcome-text"></p>
</div>

</body>
</html>
