<%--
  Created by IntelliJ IDEA.
  User: rathi-pt1424
  Date: 2/24/2017
  Time: 7:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="json" uri="http://java.sun.com/jsp/jstl/xml" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>E-Banking</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <title>Title</title>
</head>
<body>
<%
    String acc_no = request.getParameter("acc_no");
    if(acc_no != null){
%>
<script type="text/javascript">
    $(document).ready(function() {
		
        $("#account-summary-menu").click(function(){
					
			var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "acc_no").val("<%=acc_no%>");
            $('#form').append($(input));
			var input1 = $("<input>")
               .attr("type", "hidden")
               .attr("name", "item_no").val("1");
            $('#form').append($(input1));
			$('#form').submit();
            
        });
        $("#deposit-menu").click(function(){
			
			
			var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "acc_no").val("<%=acc_no%>");
            $('#form').append($(input));
			var input1 = $("<input>")
               .attr("type", "hidden")
               .attr("name", "item_no").val("2");
            $('#form').append($(input1));
			$('#form').submit();
			
            
        });
        $("#withdraw-menu").click(function(){
			
			
			var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "acc_no").val("<%=acc_no%>");
            $('#form').append($(input));
			var input1 = $("<input>")
               .attr("type", "hidden")
               .attr("name", "item_no").val("3");
            $('#form').append($(input1));
			$('#form').submit();
        });
		
        $("#add-beneficiary-menu").click(function(){
			
					
			var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "acc_no").val("<%=acc_no%>");
            $('#form').append($(input));
			var input1 = $("<input>")
               .attr("type", "hidden")
               .attr("name", "item_no").val("4");
            $('#form').append($(input1));
			$('#form').submit();
           
        });
        $("#transfer-menu").click(function(){
			
			
					
			var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "acc_no").val("<%=acc_no%>");
            $('#form').append($(input));
			var input1 = $("<input>")
               .attr("type", "hidden")
               .attr("name", "item_no").val("5");
            $('#form').append($(input1));
			$('#form').submit();
           
        });
        $("#mini-statement-menu").click(function(){

			
			var input= $("<input>").attr("type", "hidden").attr("name", "acc_no").val("<%=acc_no%>");
            $('#form').append($(input));
			var input1 = $("<input>")
               .attr("type", "hidden")
               .attr("name", "item_no").val("6");
            $('#form').append($(input1));
			$('#form').submit();
            
        });
    });
</script>

<div class="menu">

		<form action="MenuLinkAction.do" id="form">
		</form>
		
    <div class="menu-top-bar">
        <span>Accounts</span>
    </div>
    <div class="menu-item" id="account-summary-menu">
        <span>Account summary</span>
    </div>
    <div class="menu-item" id="deposit-menu">
        <span>Deposit</span>
    </div>
    <div class="menu-item" id="withdraw-menu">
        <span>Withdraw</span>
    </div>
    <div class="menu-item" id="add-beneficiary-menu">
         <span>Add beneficiary</span>
    </div>
    <div class="menu-item" id="transfer-menu">
        <span>Transfer</span>
    </div>
    <div class="menu-item" id="mini-statement-menu">
        <span>Mini statement</span>
    </div>

</div>
<%
    }%>
</body>
</html>
