<%--
  Created by IntelliJ IDEA.
  User: rathi-pt1424
  Date: 2/27/2017
  Time: 11:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<html>
<head>
    <title>E-Banking</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
</head>
<body>
<%
    String acc_no = request.getParameter("acc_no");
    System.out.println("content "+acc_no);
    int item_no = Integer.parseInt(request.getParameter("item_no"));
    String message = request.getParameter("message");
    if(acc_no != null){
%>

<script type="text/javascript">

    $(document).ready(function() {
        item_no =<%=item_no%>;

        if(item_no == 1){
			
            /*
			debugger;
			document.getElementById("account-summary-menu").style.marginRight= "0px";
             document.getElementById("account-summary-menu").style.borderTopRightRadius= "0px";
             document.getElementById("account-summary-menu").style.borderBottomRightRadius= "0px";*/
			$("#account-summary-menu").addClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Account Summary";
            $("#account-summary").show();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 2){
			
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").addClass("menu-item-selected");
			$("#withdraw-menu").removeClass("menu-item-selected");
			$("#add-beneficiary-menu").removeClass("menu-item-selected");
			$("#transfer-menu").removeClass("menu-item-selected");
			$("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Deposit";
            $("#account-summary").hide();
            $("#deposit").show();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 3){
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").addClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Withdraw";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").show();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 4){
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").addClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Add Beneficiary";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").show();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 5){
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").addClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Fund Transfer";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").show();
            $("#mini-statement").hide();
        }
        if(item_no == 6){
			
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
			$("#withdraw-menu").removeClass("menu-item-selected");
			$("#add-beneficiary-menu").removeClass("menu-item-selected");
			$("#transfer-menu").removeClass("menu-item-selected");
			$("#mini-statement-menu").addClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Transaction history";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").show();
        }


        var start = 0;
        var end = 5;
        var resultset;
        var flag = 0;
        $('#transaction_type').change(function(){
            var selected = $(this).children(":selected").text();
            switch (selected) {
                case "all transactions":
                    /*$("#myform").attr('action', 'mysql.html');
                     alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");*/
                    $("#table1 tr").remove();
                    settable_all_transaction(0);
                    break;
                case "cheques":
                    $("#table1 tr").remove();
                    settable(2);
                    break;
                case "withdraw and deposits":
                    $("#table1 tr").remove();
                    settable(3);
                    break;
                case "inbank transactions":
                    $("#table1 tr").remove();
                    settable(4);
                    break;
                case "other bank transactions":
                    $("#table1 tr").remove();
                    settable(5);
                    break;
                default:
                    $("#myform").attr('action', '#');
            }
        });
        $('#next').click(function () {
                console.log("next "+start+" "+end);
                start = start+5;
                settable_all_transaction(start);
        });
        $('#prev').click(function () {
            console.log("prev "+start+" "+end);
            start = start-5;
            settable_all_transaction(start);
        });
        function settable(transaction_type){
            $.ajax({
                type: "post",
                url: "TransactionHistoryAction.do",
                data: "acc_no=<%= acc_no%>&transaction_type="+transaction_type+"&row_no",
                success: function(msg){
                    console.log(msg);
                    resultset = msg;
                    if(msg.length<=5)
                        $('#next').hide();
                    settablevalues(start, end, resultset);
                },
                error : function(errorThrown) {
                    alert(errorThrown);
                }
            });
        }
        function settablevalues(start, end, resultset){
            $("#table1 tr").remove();
            var tr;
            tr = $('<tr/>');
            tr.append("<th>Transaction type</th>");
            tr.append("<th>Amount</th>");
            tr.append("<th>Time</th>");
            $('#table1').append(tr);

            for (var i = start; i < end; i++) {
                tr = $('<tr/>');
                tr.append("<td>" + resultset[i].type + "</td>");
                tr.append("<td>" + "₹ " + resultset[i].amount + "</td>");
                tr.append("<td>" + resultset[i].timestamp + "</td>");
                $('#table1').append(tr);
            }
        }
        function settable_all_transaction(start){
            $.ajax({
                type: "post",
                url: "TransactionHistoryAction.do",
                data: "acc_no=<%= acc_no%>&transaction_type="+1+"&row_no="+start,
                success: function(msg){
                    console.log(msg);
                    resultset = msg;
                    $('#next').show();
                    $('#prev').show();
                    if(msg.length<5){
                        $('#next').hide();
                    }
                    if(start == 0)
                        $('#prev').hide();
                    settablevalues(0, msg.length, resultset);
                },
                error : function(errorThrown) {
                    alert(errorThrown);
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $.ajax({
        type: "post",
        url: "GetAccountDetailsAction.do",
        data: "acc_no=<%=acc_no%>",
        success: function(msg){
            document.getElementById("username").innerHTML = "Username : "+msg.name;
            document.getElementById("acc_no").innerHTML = "Account number: "+msg.acc_no;
            document.getElementById("balance").innerHTML = "Total available balance  ₹"+msg.balance;
            document.getElementById("type").innerHTML = msg.type+" Account";

            var name = msg.name;
            var balance = msg.balance;
            var type = msg.type;

        },
        error : function(errorThrown) {
            alert(errorThrown);
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#inbank-submit').click(function () {
            console.log("inbank submit clicked");
            if($('#inbank-accno').val()){
                if($('#inbank-accno').val() == <%= acc_no%>){
                    document.getElementById("message").style.color = "red";
                    document.getElementById("message").textContent = "You cannot add your current account as beneficiary";
                }
                else {
                    $.ajax({
                        type: "post",
                        url: "BeneficiaryAction.do?method=addInbankBeneficiary",
                        data: "acc_no=<%= acc_no%>&to_acc_no="+$('#inbank-accno').val(),
                        success: function(msg){
                            $('#inbank').hide();
                            document.getElementById("inbank-beneficiary-success").style.display = "inline-block";
                            document.getElementById("message").innerHTML = "Successfull";
                        },
                        error : function(errorThrown) {
                            alert(errorThrown);
                        }
                    });
                }
            }
            else{
                document.getElementById("add-beneficiary-message").style.color = "red";
                document.getElementById("add-beneficiary-message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#otherbank-submit').click(function () {
            if($('#otherbank-accno').val()){

                $.ajax({
                    type: "post",
                    url: "BeneficiaryAction.do?method=addOtherbankBenefeciary",
                    data: "from_acc_no=<%= acc_no%>&acc_no="+$('#otherbank-accno').val()+"&name="+$('#otherbank-name').val()+"&bank_name="
                    +$('#otherbank-bankname').val()+"&branch_name="+$('#otherbank-branch').val()+"&ifsc="+$('#otherbank-ifsc').val(),
                    success: function(msg){
                        $('#otherbank').hide();
                        document.getElementById("otherbank-beneficiary-success").style.display = "inline-block";
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
            else{
                document.getElementById("add-beneficiary-message").style.color = "red";
                document.getElementById("add-beneficiary-message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#deposit-submit').click(function () {
            console.log("deposit submit clicked");
            if($('#deposit-amount').val()){
                $.ajax({
                    type: "post",
                    url: "WithdrawAndDepositAction.do?method=deposit",
                    data: "acc_no=<%= acc_no%>&deposit_amount="+$('#deposit-amount').val(),
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
						$('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#deposit-amount').val()+" depositted in your account");
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                        parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=1&message= ₹"+$('#deposit-amount').val()+" depositted in your account";
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#withdraw-submit').click(function () {
            console.log("deposit submit clicked");
            if($('#withdraw-amount').val()){
                $.ajax({
                    type: "post",
                    url: "WithdrawAndDepositAction.do?method=withdraw",
                    data: "acc_no=<%= acc_no%>&withdraw_amount="+$('#withdraw-amount').val(),
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
					    $('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#withdraw-amount').val()+" withdrawn from your account");
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                        parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=1&message= ₹"+$('#withdraw-amount').val()+" withdrawn from your account";
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var flag = 0;
        var beneficiary_list;
        var selected =0;
        $('#beneficiary-list').click(function(){
            if(flag == 0){
                flag = 1;
                $.ajax({
                    type: "post",
                    url: "BeneficiaryAction.do?method=getBeneficiaryList",
                    data: "acc_no=<%= acc_no%>",
                    success: function(msg){
                        beneficiary_list = msg;
                        console.log("be list "+msg);
                        var tr;
                        for (var i = 0; i < msg.length; i++) {
                            tr = $('<option/>');
                            tr.append(msg[i].to_account);
                            $('#beneficiary-list').append(tr);
                        }
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
        });
        $('#beneficiary-list').change(function(){
            selected = $(this).children(":selected").index();
            document.getElementById("to_account").textContent = "Selected Account  :"+beneficiary_list[selected-1].to_account;
        });
        $('#transfer-submit').click(function () {
            console.log("transfer submit clicked");
            x = beneficiary_list[selected-1].to_account;
            var to_account_no;
            if(beneficiary_list[selected-1].type == 1){
                to_account_no = x;
            }else{
                to_account_no = x.substr(0,x.indexOf(' '));
            }
            if($('#transfer-amount').val()){

                $.ajax({
                    type: "post",
                    url: "FundtransferAction.do",
                    data: "acc_no=<%= acc_no%>&transfer_amount="+$('#transfer-amount').val()+"&to_acc_no="+ to_account_no +"&pair_id="+ beneficiary_list[selected-1].pair_id+"&type="+beneficiary_list[selected-1].type,
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
						$('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#transfer-amount').val()+" transfered to "+to_account_no);
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                        parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=1&message= ₹"+$('#transfer-amount').val()+" transfered to "+to_account_no;
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
</script>

<div class="content">

    <form action="MenuLinkAction.do" id="message-card-form">
	</form>
    <div class="content-topbar">
        <span id ="content-name">Account summary</span>
    </div>
    <div id="account-summary">
        <div id="account-summary-main-area">
            <p id="username"></p>
            <p id="acc_no"></p>
            <p id="type"></p>
            <p id="balance"></p>
            <%
                if(message != null){%>
                    <div id="card">
                         <p><%=message%></p>
                    </div>
                <%
                }%>
        </div>
    </div>
    <div id="deposit">
        <div>
            <div id="deposit-form">
                <p>
                    <label class="label">Deposit amount</label>
                    <input type="text" id="deposit-amount">
                </p>
                <p class = "deposit-message"></p>
                <input type="submit" id="deposit-submit" value="Deposit">
            </div>
            <div class="successfull-entry" id="deposit-success">
                <p class="success-message"><b>Amount depositted successfully!!!</b></p>
            </div>
        </div>
    </div>
    <div id="withdraw">
        <div>
            <div id="withdraw-form">
                <p>
                    <label class="label">Withdraw amount</label>
                    <input type="text" id="withdraw-amount">
                </p>
                <p class = "withdraw-message"></p>
                <input type="submit" id="withdraw-submit" value="Withdraw">
            </div>
            <div class="successfull-entry" id="withdraw-success">
                <p class="success-message"><b>Amount withdrawn successfully!!!</b></p>
            </div>
        </div>
    </div>
    <div id="add-beneficiary">
        <div id="beneficiary-block">
            <div id="inbank-block">
                <div id="inbank">
                    <p>
                        <label class="label">Account number</label>
                        <input type ="text" id="inbank-accno"/>
                    </p>
                    <p id = "add-beneficiary-message"></p>
                    <input type="submit" id="inbank-submit" value="Add inbank beneficiary"/>

                </div>
                <div class="successfull-entry" id="inbank-beneficiary-success">
                    <p class="success-message"><b>Beneficiary added successfully!!!</b></p>
                </div>
            </div>
            <div id="otherbank-block">
                <div id="otherbank">
                    <p>
                        <label class="label">Account number</label>
                        <input type ="text" id="otherbank-accno"/>
                    </p>
                    <p>
                        <label class="label">Name</label>
                        <input type ="text" id="otherbank-name"/>
                    </p>
                    <p>
                        <label class="label">Bank</label>
                        <input type ="text" id="otherbank-bankname"/>
                    </p>
                    <p>
                        <label class="label">Branch</label>
                        <input type ="text" id="otherbank-branch"/>
                    </p>
                    <p>
                        <label class="label">IFSC</label>
                        <input type ="text" id="otherbank-ifsc"/>
                    </p>
                    <p id = "add-otherbank-beneficiary-message"></p>
                    <input type="submit" id="otherbank-submit" value="Add Ohter bank beneficiary"/>
                </div>
                <div class="successfull-entry" id="otherbank-beneficiary-success">
                    <p class="success-message"><b>Beneficiary added successfully!!!</b></p>
                </div>
            </div>
        </div>
    </div>
    <div id="transfer">
        <div>
            <div id="transfer-form">
                <select  id = "beneficiary-list" name="beneficiary-list">
                    <option value="select">---select---</option>
                </select>
                <p class="label" id="to_account"></p>
                <p>
                    <label class="label">Transfer amount</label>
                    <input type="text" id="transfer-amount">
                </p>
                <p class = "deposit-message"></p>
                <input type="submit" id="transfer-submit" value="Transfer">
            </div>
            <div class="successfull-entry" id="transfer-success">
                <p class="success-message"><b>Amount Transfered successfully!!!</b></p>
            </div>
        </div>
    </div>
    <div id="mini-statement">
        <div id="mini-statement-table">
            <select  id = "transaction_type" name="transaction_type">
                <option value="select">---select---</option>
                <option value="all transactions">all transactions</option>
                <option value="cheques">cheques</option>
                <option value="withdraw and deposits">withdraw and deposits</option>
                <option value="inbank transactions">inbank transactions</option>
                <option value="other bank transactions">other bank transactions</option>
            </select>
            <table id="table1"></table>
            <input type="submit" id="prev" value="Prev">
            <input type="submit" id="next" value="Next">
        </div>
    </div>
</div>
<%
    }
%>
</body>
</html>